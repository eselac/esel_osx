#!/bin/bash

cd openhaybike
while true; do
    python3 -m pipenv run python scripts/server.py
    sleep 10
done
