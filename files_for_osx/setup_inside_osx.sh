#!/bin/bash

# This script should be fetched from gitlab by setup_osx.sh and run as root

## Optimizations

# Skip GUI login sreen
sudo defaults write com.apple.loginwindow autoLoginUser -bool true

# massively increase virtualized macOS by disabling spotlight.
sudo mdutil -i off -a

# since you can't use spotlight to find apps, you can renable with
# sudo mdutil -i on -a

# check if enabled (should contain `serverperfmode=1`)
sudo nvram boot-args

# turn on
sudo nvram boot-args="serverperfmode=1 $(nvram boot-args 2>/dev/null | cut -f 2-)"

# turn off
sudo nvram boot-args="$(nvram boot-args 2>/dev/null | sed -e $'s/boot-args\t//;s/serverperfmode=1//')"

sudo defaults write com.apple.Accessibility DifferentiateWithoutColor -int 1
sudo defaults write com.apple.Accessibility ReduceMotionEnabled -int 1
sudo defaults write com.apple.universalaccess reduceMotion -int 1
sudo defaults write com.apple.universalaccess reduceTransparency -int 1
sudo defaults write com.apple.Accessibility ReduceMotionEnabled -int 1

sudo /usr/bin/defaults write .GlobalPreferences MultipleSessionsEnabled -bool TRUE

sudo defaults write "Apple Global Domain" MultipleSessionsEnabled -bool true

sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticDownload -bool false
sudo defaults write com.apple.SoftwareUpdate AutomaticCheckEnabled -bool false
sudo defaults write com.apple.commerce AutoUpdate -bool false
sudo defaults write com.apple.commerce AutoUpdateRestartRequired -bool false
sudo defaults write com.apple.SoftwareUpdate ConfigDataInstall -int 0
sudo defaults write com.apple.SoftwareUpdate CriticalUpdateInstall -int 0
sudo defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 0
sudo defaults write com.apple.SoftwareUpdate AutomaticDownload -int 0

sudo defaults write com.apple.loginwindow DisableScreenLock -bool true

git clone https://gitlab.com/eselac/openhaybike.git
mv config.toml openhaybike/config.toml

cd openhaybike
python3 -m pip install pipenv
python3 -m pipenv install

echo alpine | python3 -m pipenv run python scripts/get_icloud_key.py | tail -n 1 >> config.toml
