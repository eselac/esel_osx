# Esel_OSX

## What Esel_OSX is

With minimal setup, get a running OSX instance that uses [openhaybike](https://gitlab.com/eselac/openhaybike) to retrieve locations from fake BLE airtags (TODO: Document!) and upload them as trackers into our [Cykel fork](https://gitlab.com/eselac/cykel).

## Setup

Clone an image, we use this one:
```
wget https://images.sick.codes/mac_hdd_ng_auto_monterey.img
```
Copy it to a backup location too.

Clone the repo in your host machine. Run Docker setup in host:
```
./run_first.sh <absolute path to .img>
```
If it asks for your password, it's always `alpine`.

Connect to `localhost:5999` using your VNC and once MacOS is ready, log into iCloud and start this right away in OSX:
```
xcode-select --install
```
Put your cykel info (API endpoint is the URL that ends in `/api` and the key can be generated inside of cykel) into `files_for_osx/config.toml.sample`.
Run in your host:
```
./run_second.sh
```

Run inside osx (the terminal is in Finder -> Applications -> Utilities -> Terminal):
```
# wait for xcode install to finish
./setup_inside_osx.sh
```

Now start the server inside of the OSX docker container using:
```
./run_server.sh
```
