#!/bin/bash

# Setup OSX
docker run -it --privileged --device /dev/kvm -p 5999:5999 -p 50922:10022 -v \
    "$1:/image" -v /tmp/.X11-unix:/tmp/.X11-unix \
    -e "DISPLAY=${DISPLAY:-:0.0}" -e "USERNAME=user" -e "PASSWORD=alpine" -e \
    GENERATE_UNIQUE=true -e EXTRA="-display none -vnc 0.0.0.0:99" \
    sickcodes/docker-osx:naked
